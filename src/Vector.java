import java.util.Random;

public class Vector {

    public static int generateSizeOfVector() {
        return (int) (Math.random() * 6 + 5);
    }


    public static void insertDataToVector(double[] vector) {
        try {
            for (int i = 0; ; i++) {
                vector[i] = Math.random();
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Zlapano wyjatek ArrayIndexOutOfBoundsException");
        }
    }

    public static void showVector(double[] vector) {
        try {
            for (int i = 0; ; i++) {
                System.out.println("TAB[" + i + "]: " + vector[i]);
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Zlapano wyjatek ArrayIndexOutOfBoundsException");
            System.out.println("Koniec wyswietlania danych!");
        }
    }

    public static void main(String[] args) {
        System.out.println();
        System.out.println("Generuje rozmiar wektora...");
        double[] vector = new double[generateSizeOfVector()];
        System.out.println("Wprowadzam dane do wektora...");
        insertDataToVector(vector);
        System.out.println("Wyswietlam zawartosc wektora");
        showVector(vector);
    }
}

